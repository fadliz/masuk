import React, { Component, useState } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity, Alert
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import { TextInput, Text, Button, Divider, HelperText } from 'react-native-paper';

const Form = ({ route, navigation }) => {
    const axios = require('axios').default;
    const { name } = route.params;
    const [conf, setConf] = useState('');
    const [pass, setPass] = useState('');
    const [email, setEmail] = useState('');
    const [nama, setNama] = useState('');
    const [username, setUsername] = useState('');
    const regis = () => {
        if (name == 'register') {
            return (<>
                <TextInput style={styles.inputBox}
                    mode="outlined"
                    textColor='black'
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Confirm Password"
                    placeholderTextColor="black"
                    selectionColor="black"
                    secureTextEntry={true}
                    onChangeText={conf => setConf(conf)}
                    defaultValue={conf}
                />
                <TextInput style={styles.inputBox}
                    mode="outlined"
                    textColor='black'
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Username"
                    placeholderTextColor="black"
                    selectionColor="black"
                    onChangeText={username => setUsername(username)}
                    defaultValue={username}
                />
                <TextInput style={styles.inputBox}
                    mode="outlined"
                    textColor='black'
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Nama"
                    placeholderTextColor="black"
                    selectionColor="black"
                    onChangeText={nama => setNama(nama)}
                    defaultValue={nama}
                /></>
            );
        }
    }

    const valid = (mel, pas, conf) => {
        if (name == 'login') {
            bener(mel, pas)
        } else if (pas.length <= 0 || mel.length <= 0) {
            kosong();
        } else if (name == 'register') {
            register();
        } else {
            salah();
        }
    }

    const register = async (e) => {
        axios
            .post('https://mighty-eyrie-29994.herokuapp.com/api/auth/signup', {
                name: nama,
                username: username,
                email: email,
                password: pass,
                passwordConfirmation: conf,


            })
            .then(function (res) {
                console.log('res ' + res.data);
                navigation.navigate('masuk', { name: 'login' });
                alert('data tersimpan');
            })
            .catch(function (error) {
                console.log(JSON.stringify(error.response, null, 2));
                alert(SON.stringify(error.response.data.error.message, null, 2));
            });

    }

    const kosong = () => {
        Alert.alert('Error', 'Input cannot be empty', [
            { text: "OK", onPress: () => console.log("OK Pressed") }
        ],
            { cancelable: true });
    }

    const salah = (mel, pas, conf) => {
        Alert.alert('Error', 'Confirm password is not correct', [
            { text: "OK", onPress: () => console.log("OK Pressed") }
        ],
            { cancelable: true });
    }

    const hasErrors = () => {
        return !email.includes('@', '.');
    };


    const bener = async (e) => {
        axios.post('https://mighty-eyrie-29994.herokuapp.com/api/auth/signin', {
            username: email,
            password: pass,
        })
            .then(function (res) {
                console.log(res.data);
                storeToken(res.data.accessToken, res.data.roles[0]);
                // postToken()
                navigation.navigate('masuk', {
                    screen: 'masuk',
                    params: {
                        screen: 'tabdua',
                        params: {
                            email: email,
                            pass: pass,
                        },
                    },
                });
            })
            .catch(function (error) {
                console.log(JSON.stringify(error.response.data, null, 2));
                alert(error);
            });
    }

    const storeToken = async (token, role) => {
        try {
            await AsyncStorage.setItem(
                'token',
                token
            );
            await AsyncStorage.setItem(
                'role',
                role
            );
            console.log('tokenLogin' + await AsyncStorage.getItem('token'));
            console.log('roleLogin' + await AsyncStorage.getItem('role'));
        } catch (error) {
            console.log(error)
        }
    };

    // const postToken = async (e) => {
    //     const header = {
    //         'Content-Type': 'application/json',
    //         'authorization': AsyncStorage.getItem('token')
    //     }
    //     axios.get('hhttps://mighty-eyrie-29994.herokuapp.com/api/test/user', {
    //         headers: header
    //     })
    //         .then(function (res) {
    //             console.log(res);
    //         })
    //         .catch(function (error) {
    //             console.log(error);
    //             alert(error);
    //         });

    // }

    return (
        <View style={styles.container}>
            <TextInput style={styles.inputBox}
                mode="outlined"
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholder="Email"
                placeholderTextColor="black"
                selectionColor="black"
                keyboardType="email-address"
                onChangeText={email => setEmail(email)}
                defaultValue={email}
            />
            <HelperText type="error" visible={hasErrors()}>
                Email address is invalid!
            </HelperText>
            <TextInput style={styles.inputBox}
                mode="outlined"
                backgroundColor=''
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholder="Password"
                secureTextEntry={true}
                placeholderTextColor="black"
                onChangeText={pass => setPass(pass)}
                defaultValue={pass}
            />
            {regis()}
            <Divider />
            <Button
                style={{ marginVertical: 10, width: '69%', alignSelf: 'center' }}
                title="Login"
                mode="contained"
                icon="camera"
                onPress={() => valid(email, pass, conf)}

            >{name}</Button>
        </View>
    );
}
export default Form;

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    inputBox: {
        width: 300,
        backgroundColor: 'rgba(255, 255,255,0.2)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: 'black',
        marginVertical: 10
    },
    button: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    }

});