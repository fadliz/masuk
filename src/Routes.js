import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import React, { Component } from 'react';
import HomeScreen from './pages/HomeScreen';
import 'react-native-gesture-handler';
import Form from './components/Form';
import Masuk from './pages/Masuk';
import { useWindowDimensions } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Tabdua from './pages/Tabdua'
import Tabsatu from './pages/Tabsatu'
import UpdateUser from './pages/UpdateUser'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
const Tab = createMaterialBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

function Routes() {
    return (
        <>
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen
                        name="Home"
                        component={HomeScreen}
                        options={{ title: 'Welcome' }}
                    />
                    <Stack.Screen name="tabsatu" component={MyTabs} />
                    <Stack.Screen name="form" component={Form} />
                    <Stack.Screen name="masuk" component={drawer} />
                </Stack.Navigator>
                {/* <Drawer.Navigator>
                        <Drawer.Screen name="masuk" component={Masuk} />
                    </Drawer.Navigator> */}
            </NavigationContainer>
        </>
    );
}

function updateUser() {
    return (
        <>
            <Stack.Navigator>
                <Stack.Screen
                    name="home"
                    component={Tabsatu}
                    options={{ title: 'Data User' }}
                />
                <Stack.Screen
                    name="updateUser"
                    component={UpdateUser}
                    options={{ title: 'Data User' }}
                />

            </Stack.Navigator>
            {/* <Drawer.Navigator>
                        <Drawer.Screen name="masuk" component={Masuk} />
                    </Drawer.Navigator> */}
        </>
    );
}

function MyTabs() {
    return (
        <Tab.Navigator
            tabBarOptions={{ style: { paddingBottom: 10 } }}
            initialRouteName="Home"
            activeColor="black"
            inactiveColor="#e5e5e5"
            barStyle={{ backgroundColor: 'white' }}
        >
            <Tab.Screen
                name="tabdua"
                options={{
                    tabBarIcon: () =>
                        (<Ionicons size={20} name="camera" activeColor="black" inactiveColor="#DBD9D0" />)
                }}
                component={Masuk}
            />
            <Tab.Screen
                name="tasatu"
                component={updateUser}
                options={{
                    tabBarIcon: () =>
                        (<Ionicons size={20} name="calendar" activeColor="black" inactiveColor="#DBD9D0">

                        </Ionicons>)
                }} />
            <Tab.Screen
                name="tabhehe"
                options={{
                    tabBarIcon: () =>
                        (<Ionicons size={20} name="camera" activeColor="black" inactiveColor="#DBD9D0" />)
                }}
                component={Tabdua}

            />
        </Tab.Navigator>
    );
}
function drawer() {
    return (
        <>
            <Drawer.Navigator drawerStyle={{ backgroundColor: '#c6cbef' }}>
                <Drawer.Screen name="masuk" component={MyTabs} />
            </Drawer.Navigator>
        </>
    )
}
export default Routes;