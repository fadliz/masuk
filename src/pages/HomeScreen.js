import AsyncStorage from '@react-native-community/async-storage';
import React, { Component, useState, useEffect } from 'react';
import { Button, Divider } from 'react-native-paper';
import Masuk from './Masuk';



const HomeScreen = ({ navigation }) => {

    const [token, setToken] = useState(null);
    const getToken = async () => {
        try {
            setToken(await AsyncStorage.getItem('token'))
            console.log("token Homescreen=" + token);
        } catch (e) {
            console.log('error' + e);
        }
    }


    useEffect(() => {
        getToken()
        if (token != null) {
            navigation.navigate('masuk', {
                screen: 'masuk',
                params: {
                    screen: 'tabdua',
                    params: {
                        email: 'email',
                        pass: 'pass',
                    },
                },
            });
        }
    }, [token]);

    const cobaJe = (e) => {
        if (isSignIn == false) {
            navigation.navigate('masuk', {
                screen: 'masuk',
                params: {
                    screen: 'tabdua',
                    params: {
                        email: 'email',
                        pass: 'pass',
                    },
                },
            });
        } else {
            navigation.navigate('form', { name: e })
        }
    }
    const isSignIn = async () => {
        try {
            if (token == null || token == '') {
                return false;
            } else {
                return true;
            }
        } catch (err) {
            alert(err);
        }
    }
    return (
        <>
            <Button
                style={{ marginVertical: 10, width: '69%', alignSelf: 'center' }}
                title="Login"
                mode="contained"
                icon="camera"
                onPress={() =>
                    cobaJe('login')
                }
            >LOGIN</Button>

            <Divider />
            <Button
                style={{ marginVertical: 10, width: '69%', alignSelf: 'center' }}
                title="Register"
                mode="contained"
                icon="camera"
                onPress={() =>
                    cobaJe('register')
                }
            >REGISTER</Button>
        </>
    );


};
export default HomeScreen;