import React, { Component } from 'react';
import { Text, View } from 'react-native';


const Tabsatu = ({ route, navigation }) => {
    const { id } = route.params;
    useEffect(() => {
        fetch('https://api.themoviedb.org/3/movie/' + id + '?api_key=7a857a153e163f1f3b4d1eeb22034316&language=en-US')
            .then((response) => response.json())
            .then((json) => setData(json))
            .catch((error) => console.error(error))
            .finally(() => setLoading(false));
    }, []);
    return (
        <>
            <FlatList
                horizontal={true}
                data={data}
                keyExtractor={({ id }, index) => id}
                renderItem={({ item }) => (
                    <ScrollView>
                        <Image style={style.gambar} source={{ uri: 'https://image.tmdb.org/t/p/w500' + item.poster_path }} />
                        <Title style={{ color: 'black', textAlign: 'center' }}>{item.title}</Title>
                        <Text style={{ color: 'black', textAlign: 'center' }}>{item.overview}</Text>
                    </ScrollView>

                )}
            />
        </>
    );

}
export default Tabsatu;