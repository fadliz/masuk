import React, { Component, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, TextInput, Button } from 'react-native-paper';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';



const UpdateUser = ({ route, navigation }) => {
    // const { email, pass } = route.params;
    const { rutId, rutNama, rutUsername, rutEmail } = route.params;
    const [token, setToken] = useState(null);
    const [role, setRole] = useState(null);
    console.log(rutId);
    const [conf, setConf] = useState('');
    const [pass, setPass] = useState('');
    const [email, setEmail] = useState('');
    const [nama, setNama] = useState('');
    const [username, setUsername] = useState('');

    const getToken = async () => {
        try {
            setToken(await AsyncStorage.getItem('token'))
            console.log(token);
            setRole(await AsyncStorage.getItem('role'))
            // console.log("token =" + token);
        } catch (e) {
            console.log('error' + e);
        }
    }

    const update = () => {

        getToken();
        const header = {
            'authorization': token,
        }
        const body = {
            name: nama,
            username: username,
            email: email,
            password: pass
        }
        axios
            .put(`https://mighty-eyrie-29994.herokuapp.com/api/users/${rutId}`, {
                headers: header,
                body

            })
            .then(function (res) {
                console.log('res ' + res.data);
                navigation.navigate('home');
                alert('Data Updated');
            })
            .catch(function (error) {
                console.log(JSON.stringify(error.response, null, 2));
                alert(JSON.stringify(error.response, null, 2));
            });

    }
    return (
        <>
            <View >
                <Text>UPDATE AKUN HAYYUK</Text>
                <TextInput style={styles.inputBox}
                    label="Username"
                    mode="outlined"
                    textColor='black'
                    underlineColor='transparent'
                    placeholderTextColor="black"
                    selectionColor="black"
                    onChangeText={username => setUsername(username)}
                    defaultValue={rutUsername}
                />
                <TextInput style={styles.inputBox}
                    mode="outlined"
                    textColor='black'
                    underlineColorAndroid='rgba(0,0,0,0)'
                    label="email"
                    placeholderTextColor="black"
                    selectionColor="black"
                    onChangeText={email => setEmail(email)}
                    defaultValue={rutEmail}
                />
                <TextInput style={styles.inputBox}
                    mode="outlined"
                    textColor='black'
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholderTextColor="black"
                    selectionColor="black"
                    onChangeText={nama => setNama(nama)}
                    defaultValue={rutNama}
                />
                <TextInput style={styles.inputBox}
                    mode="outlined"
                    textColor='black'
                    underlineColorAndroid='rgba(0,0,0,0)'
                    label="password"
                    placeholderTextColor="black"
                    selectionColor="black"
                    onChangeText={pass => setPass(pass)}
                    defaultValue={pass}
                />


                <Button
                    style={{ marginVertical: 10, width: '69%', alignSelf: 'center' }}
                    title="Login"
                    mode="contained"
                    icon="delete"
                    onPress={() => update()}
                >UPDATE DATA</Button>

            </View>
        </>
    );

}
export default UpdateUser;

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    inputBox: {
        width: 300,
        backgroundColor: 'rgba(255, 255,255,0.2)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: 'black',
        marginVertical: 10
    },
    button: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    }

});