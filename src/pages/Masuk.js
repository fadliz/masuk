import React, { Component, useState, useEffect, ActivityIndicator } from 'react';
import { View, ScrollView, Image, StyleSheet, Modal, TouchableOpacity, FlatList } from 'react-native';
import { Text, Button, Title, Subheading, Divider } from 'react-native-paper';

import AsyncStorage from '@react-native-community/async-storage';


const Login = ({ route, navigation }) => {
    // const { email, pass } = route.params;

    const [modalVisible, setModalVisible] = useState(false);
    const [gambarSrc, setGambarSrc] = useState("as");
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('https://api.themoviedb.org/3/movie/popular?api_key=7a857a153e163f1f3b4d1eeb22034316&language=en-US&page=1')
            .then((response) => response.json())
            .then((json) => setData(json.results))
            .catch((error) => console.error(error))
            .finally(() => setLoading(false));
    }, []);

    const keluar = () => {
        AsyncStorage.clear()
        navigation.navigate('Home')
    }

    return (
        <>
            <View style={{ backgroundColor: '#e5e5e5', width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }} >
                <View style={{ backgroundColor: '#14213d', width: '100%' }}>
                    <Title style={{ textAlign: 'center', color: 'white' }}>SAPA SURUH LOGIN</Title>
                    {/* <Subheading style={{ textAlign: 'center', color: 'white' }}>{JSON.stringify(email)}</Subheading>
                    <Subheading style={{ textAlign: 'center', color: 'white' }}>{JSON.stringify(pass)}</Subheading> */}
                    <Subheading style={{ textAlign: 'center', color: 'white' }}>as</Subheading>

                </View>


                <FlatList
                    horizontal={true}
                    data={data}
                    keyExtractor={({ id }, index) => id}
                    renderItem={({ item }) => {
                        return (
                            <ScrollView>
                                <TouchableOpacity
                                    style={{ alignItems: 'center', width: 169, padding: 10 }}

                                    onPress={() => {
                                        setModalVisible(true),
                                            setGambarSrc('https://image.tmdb.org/t/p/w500' + item.poster_path)
                                    }
                                    }>
                                    <Image style={style.gambar} source={{ uri: 'https://image.tmdb.org/t/p/w500' + item.poster_path }} />
                                </TouchableOpacity>
                                <TouchableOpacity
                                    // onPress={() => {
                                    //     navigation.navigate('detail', { id: item.id })
                                    // }}
                                    style={{ alignItems: 'center', width: 169, padding: 10 }}>
                                    <Title style={{ color: 'black', textAlign: 'center' }}>{item.title}</Title>
                                    <Text style={{ color: 'black', textAlign: 'center' }}>{item.overview}</Text>
                                </TouchableOpacity>
                            </ScrollView>

                        )

                    }}
                />

                <Button

                    style={{ marginVertical: 50, width: '69%', alignSelf: 'center' }}
                    title="Logout"
                    mode="contained"
                    icon="camera"
                    onPress={() =>
                        keluar()
                    }
                >Logout</Button>

                <Modal
                    style={style.modalView}
                    animationType="fade"
                    transparent={true}
                    visible={modalVisible}
                >
                    <TouchableOpacity
                        style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }}
                        onPress={() => {
                            setModalVisible(!modalVisible)

                        }}>
                        <Image style={{ width: '69%', height: '69%' }} source={{ uri: gambarSrc }} />
                    </TouchableOpacity>
                </Modal>
            </View>
        </>
    );

}



export default Login;
const style = StyleSheet.create({
    gambar: {
        flex: 1,
        width: 150,
        height: 200,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 7,
        borderRadius: 19,
    }, centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});