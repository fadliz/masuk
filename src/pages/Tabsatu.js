import React, { Component, useState, useEffect } from 'react';
import { Text, View, FlatList, StyleSheet } from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { Divider, Button } from 'react-native-paper';

const Tabsatu = ({ route, navigation }) => {
    // const { email, pass } = route.params;
    const [token, setToken] = useState(null);
    const [role, setRole] = useState(null);
    const [data, setData] = useState([{
        name: "", username: "", email: "", roles: [{ id: 0 }]
    }]);
    const getToken = async () => {
        try {
            setToken(await AsyncStorage.getItem('token'))
            setRole(await AsyncStorage.getItem('role'))
            // console.log("token =" + token);
        } catch (e) {
            console.log('error' + e);
        }
    }

    useEffect(() => {
        getToken();
        console.log("setelah = " + token);
        const postToken = async (e) => {
            const header = {
                'Content-Type': 'application/json',
                'Authorization': token,
            }
            axios
                .get('https://mighty-eyrie-29994.herokuapp.com/api/users', {
                    headers: header
                })
                .then(function (res) {
                    console.log(res.data.user);
                    setData(res.data.user);
                })
                .catch(function (error) {
                    console.log(JSON.stringify(error.response.data, null, 2));
                    alert(error);
                })
        }
        if (token != null) {
            postToken()
        }
    }, [token]);

    const adminButton = (id, namaaa, email, username) => {
        if (role == 'ADMIN') {
            return (
                <>
                    <Button
                        style={{ marginVertical: 10, width: '69%', alignSelf: 'center' }}
                        title="Login"
                        mode="contained"
                        icon="camera"
                        onPress={() => navigation.navigate('updateUser', { rutId: id, rutName: namaaa, rutEmail: email, rutUsername: username })}
                    >UPDATE</Button>
                    <Button
                        style={{ marginVertical: 10, width: '69%', alignSelf: 'center' }}
                        title="Login"
                        mode="contained"
                        icon="delete"
                        onPress={() => deleteData(id)}
                    >DELETE</Button>
                </>
            )
        }
    }

    const deleteData = (id) => {
        getToken();
        const postToken = async (e) => {
            const header = {
                'Content-Type': 'application/json',
                'Authorization': token,
            }
            axios
                .delete(`https://mighty-eyrie-29994.herokuapp.com/api/users/${id}`, {
                    headers: header
                })
                .then(function (res) {
                    console.log(res.data.user);
                    setData(res.data.user);
                })
                .catch(function (error) {
                    console.log(JSON.stringify(error.response.data, null, 2));
                    alert(error);
                })
        }
        if (token != null) {
            postToken()
        }
    }
    return (
        <>
            <View >
                <Text>halaman tab satu</Text>
                <FlatList
                    data={data}
                    keyExtractor={({ username }, index) => username}
                    renderItem={({ item }) => {
                        return (
                            <>
                                <Text style={{ textAlign: 'center', marginTop: 15 }}>{item.name}</Text>
                                <Text style={{ textAlign: 'center' }}>{item.username}</Text>
                                <Text style={{ textAlign: 'center', marginBottom: 15 }}>{item.email} </Text>
                                {adminButton(item.id, item.name, item.email, item.username)}
                                <Divider style={{ height: 2, }} />
                            </>
                        )

                    }}
                />

            </View>
        </>
    );

}
export default Tabsatu;
const style = StyleSheet.create({
    gambar: {
        flex: 1,
        width: 150,
        height: 200,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 7,
        borderRadius: 19,
    }, centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});