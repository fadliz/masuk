/**
 * @format
 */
import { configureFonts, DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import * as React from 'react';
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';

const theme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
    },

    roundness: 10,

};

const fontConfig = {
    default: {
        regular: {
            fontFamily: 'arial',
            fontWeight: 'normal',
        },
        medium: {
            fontFamily: 'sans-serif-medium',
            fontWeight: 'normal',
        },
        light: {
            fontFamily: 'sans-serif-light',
            fontWeight: 'normal',
        },
        thin: {
            fontFamily: 'sans-serif-thin',
            fontWeight: 'normal',
        },
    },
};

export default function Main() {
    return (
        <PaperProvider theme={theme}>
            <App />
        </PaperProvider>
    );
}

AppRegistry.registerComponent(appName, () => Main);
